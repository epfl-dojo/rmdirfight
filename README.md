# rmdir fight

Two teams are fighting: the team A have to create a directory strucure with some files in it. Team B have to delete empty repositories. Any languages is allowed.

It's not allowed:
1. To delete a file (for team A)
1. To jump out the ring (e.g. damange the VM or any other EPFL properties)

# Team A
`pwgen 1000 1000 | sed 's/\(.\)/\1\//g' | xargs mkdir -p`

# Team B
`find . -depth -type d -empty -delete`